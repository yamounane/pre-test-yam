#  PM Kata 
Bank account Kata for PriceMinister implemented in Java & Maven  

# Getting started 

## Project requirements 

 * Deposit operation 
 * Withdrawal operation
 * Handling balance policy on operations 
 * Reporting balance

## Installing

To build and install the jar in your local repository, just run :

```sh
$ cd [PATH_TO_YOUR_PROJECT]
$ mvn clean install
```

## Running the test   
To launch the tests, you need to run :

```sh
$ cd [PATH_TO_YOUR_PROJECT]
$ mvn test
```

## Contributors

* **Yassine Amounane** - [yamounane](https://github.com/yamounane)