package com.priceminister.account.implementation;

import com.priceminister.account.Account;
import com.priceminister.account.AccountRule;
import com.priceminister.account.exceptions.AccountException;
import com.priceminister.account.exceptions.IllegalBalanceException;
import com.priceminister.account.exceptions.IllegalOperationException;

public class CustomerAccount implements Account {

	private Double balance = 0d;

	public void add(Double addedAmount) throws AccountException {
		if (isInvalid(addedAmount)) {
			throw new IllegalOperationException(addedAmount);
		}

		balance += addedAmount;
	}

	public Double getBalance() {
		return balance;
	}

	public Double withdrawAndReportBalance(Double withdrawnAmount, AccountRule rule) throws AccountException {
		if (rule != null) {
			return withdraw(withdrawnAmount, rule);
		}
		throw new IllegalStateException("No policy set for withdrawal operation");
	}

	private Double withdraw(Double withdrawnAmount, AccountRule rule) throws AccountException {
		if (!isInvalid(withdrawnAmount)) {
			Double expectedBalance = balance - withdrawnAmount;

			if (rule.withdrawPermitted(expectedBalance)) {
				balance = expectedBalance;
				return getBalance();
			}

			throw new IllegalBalanceException(expectedBalance);
		}

		throw new IllegalOperationException(withdrawnAmount);
	}

	private static boolean isInvalid(Double amount) {
		if (amount != null) {
			return Double.compare(amount, 0.0) < 0;
		}

		return true;
	}

}
