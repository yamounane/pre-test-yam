package com.priceminister.account.exceptions;

/**
 * 
 * @author yamounane
 */
public class IllegalOperationException extends AccountException {

	private static final long serialVersionUID = 6202460506817462718L;

	public IllegalOperationException(Double illegalBalance) {
		super(illegalBalance);
	}

	public String toString() {
		return "Illegal operation : " + super.getBalance();
	}
}
