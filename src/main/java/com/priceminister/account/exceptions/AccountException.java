package com.priceminister.account.exceptions;

/**
 * 
 * @author yamounane
 */
public class AccountException extends Exception {

	private static final long serialVersionUID = -1207438040405537256L;

	private Double balance;

	public AccountException(Double illegalBalance) {
		balance = illegalBalance;
	}

	public Double getBalance() {
		return balance;
	}

}