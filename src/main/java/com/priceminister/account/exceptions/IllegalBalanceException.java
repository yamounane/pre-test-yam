package com.priceminister.account.exceptions;

public class IllegalBalanceException extends AccountException {

	private static final long serialVersionUID = -9204191749972551939L;

	public IllegalBalanceException(Double illegalBalance) {
		super(illegalBalance);
	}

	public String toString() {
		return "Illegal account balance: " + super.getBalance();
	}

}
