package com.priceminister.account;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.Before;
import org.junit.Test;

import com.priceminister.account.exceptions.AccountException;
import com.priceminister.account.exceptions.IllegalBalanceException;
import com.priceminister.account.exceptions.IllegalOperationException;
import com.priceminister.account.implementation.CustomerAccount;
import com.priceminister.account.implementation.CustomerAccountRule;

/**
 * Please create the business code, starting from the unit tests below.
 * Implement the first test, the develop the code that makes it pass. Then focus
 * on the second test, and so on.
 * 
 * We want to see how you "think code", and how you organize and structure a
 * simple application.
 * 
 * When you are done, please zip the whole project (incl. source-code) and send
 * it to recrutement-dev@priceminister.com
 * 
 */
public class CustomerAccountTest {

	Account customerAccount;
	AccountRule rule;

	Double ONE_THOUSAND = 1000.0;
	Double TWO_THOUSAND = 2000.0;
	Double MINUS_ONE_THOUSAND = -1000.0;

	@Before
	public void setUp() throws Exception {
		customerAccount = new CustomerAccount();
		rule = new CustomerAccountRule();
	}

	@Test
	public void should_account_balance_to_zero_when_empty() {
		assertThat(customerAccount.getBalance()).isEqualTo(0d);
	}

	@Test
	public void shpuld_balance_is_one_thousand_when_deposit_of_one_thousand() throws AccountException {
		customerAccount.add(ONE_THOUSAND);

		assertThat(customerAccount.getBalance()).isEqualTo(ONE_THOUSAND);
	}

	@Test
	public void shpuld_balance_stay_unchanged_when_deposit_of_zero_occurs() throws AccountException {
		customerAccount.add(ONE_THOUSAND);
		customerAccount.add(0.0);

		assertThat(customerAccount.getBalance()).isEqualTo(ONE_THOUSAND);
	}

	@Test
	public void shpuld_throw_exception_when_deposit_of_minus_one_thousand() {
		assertThatThrownBy(() -> customerAccount.add(MINUS_ONE_THOUSAND)).isInstanceOf(IllegalOperationException.class);
	}

	@Test
	public void shpuld_deposit_throw_exception_when_deposit_of_null_amount() {
		assertThatThrownBy(() -> customerAccount.add(null)).isInstanceOf(IllegalOperationException.class);
	}

	@Test
	public void should_balance_be_two_thousand_when_withdraw_one_thousand_of_three_thousand() throws AccountException {
		customerAccount.add(ONE_THOUSAND);
		customerAccount.add(ONE_THOUSAND);
		customerAccount.add(ONE_THOUSAND);

		Double expectedBalance = customerAccount.withdrawAndReportBalance(ONE_THOUSAND, rule);

		assertThat(expectedBalance).isEqualTo(TWO_THOUSAND);
	}

	@Test
	public void should_balance_stay_unchanged_when_withdraw_zero_of_one_thousand() throws AccountException {
		customerAccount.add(ONE_THOUSAND);

		Double expectedBalance = customerAccount.withdrawAndReportBalance(0.0, rule);

		assertThat(expectedBalance).isEqualTo(ONE_THOUSAND);
	}

	@Test
	public void should_exception_is_raised_when_illegal_withdrawal() throws AccountException {
		assertThatThrownBy(() -> customerAccount.withdrawAndReportBalance(ONE_THOUSAND, rule))
				.isInstanceOf(IllegalBalanceException.class);
	}

	@Test
	public void should_exception_is_raised_when_withdrawal_of_negative_amount() throws AccountException {
		customerAccount.add(ONE_THOUSAND);
		customerAccount.add(ONE_THOUSAND);
		customerAccount.add(ONE_THOUSAND);

		assertThatThrownBy(() -> customerAccount.withdrawAndReportBalance(MINUS_ONE_THOUSAND, rule))
				.isInstanceOf(IllegalOperationException.class);
	}

	@Test
	public void should_exception_is_raised_when_withdrawal_with_null_rule() throws AccountException {
		customerAccount.add(ONE_THOUSAND);
		customerAccount.add(ONE_THOUSAND);
		customerAccount.add(ONE_THOUSAND);

		assertThatThrownBy(() -> customerAccount.withdrawAndReportBalance(ONE_THOUSAND, null))
				.isInstanceOf(IllegalStateException.class);
	}

	@Test
	public void should_exception_is_raised_when_withdrawal_with_null_amount() throws AccountException {
		customerAccount.add(ONE_THOUSAND);
		customerAccount.add(ONE_THOUSAND);
		customerAccount.add(ONE_THOUSAND);

		assertThatThrownBy(() -> customerAccount.withdrawAndReportBalance(null, null))
				.isInstanceOf(IllegalStateException.class);
	}

}
