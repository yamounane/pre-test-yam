package com.priceminister.account;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.priceminister.account.implementation.CustomerAccountRule;

/**
 * Test class for {@link CustomerAccountRule}
 * 
 * @author yamounane
 */
public class AccountRuleTest {

	AccountRule rule;
	Double MINUS_ONE_THOUSAND = -1000.0;
	Double ONE_THOUSAND = 1000.0;

	@Before
	public void setUp() throws Exception {
		rule = new CustomerAccountRule();
	}

	@Test
	public void should_withdraw_be_permitted_when_amount_positive() {
		assertThat(rule.withdrawPermitted(ONE_THOUSAND)).isEqualTo(true);
	}

	@Test
	public void should_withdraw_not_be_permitted_when_amount_negative() {
		assertThat(rule.withdrawPermitted(MINUS_ONE_THOUSAND)).isEqualTo(false);
	}

	@Test
	public void should_withdraw_not_be_permitted_when_amount_is_null() {
		assertThat(rule.withdrawPermitted(null)).isEqualTo(false);
	}

}
